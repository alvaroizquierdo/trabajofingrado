package clases;

import java.util.Scanner;

public class Simulador{
	
	//Claves del canal en ThingSpeak de cada estudiante
	private final static String THINGSPEAK_API_KEY_61343 = "6I3NISMSLETFUBZT", THINGSPEAK_API_KEY_66194 = "5EWC7RZNH434456Q";
	
	//N�mero de canal en ThingSpeak de cada estudiante
	private final static int CHANNEL_ID_61343 = 61343, CHANNEL_ID_66194 = 66194;
	
	//Objetos de la clase Estudiante, para poder mostrar la informaci�n de cada uno
	private static Estudiante estudiante1, estudiante2;
	
	//Objetos de la clase Temporizador. Uno por cada variable y por cada estudiante para que no se solapen los tiempos
	private static Temporizador temporizadorEstudio1 = new Temporizador(), temporizadorEstudio2 = new Temporizador(),
			temporizadorDormir1 = new Temporizador(), temporizadorDormir2 = new Temporizador(), 
			temporizadorDescanso1 = new Temporizador(), temporizadorDescanso2 = new Temporizador(),
			temporizadorTecnologias1 = new Temporizador(), temporizadorTecnologias2 = new Temporizador(),
			temporizadorSimEst = new Temporizador(), temporizadorSimEst2 = new Temporizador(), 
			temporizadorSimDor1 = new Temporizador(), temporizadorSimDor2 = new Temporizador(),
			temporizadorSimDesc1 = new Temporizador(), temporizadorSimDesc2 = new Temporizador(),
			temporizadorSimTecn1 = new Temporizador(), temporizadorSimTecn2 = new Temporizador();
	
	//Canales de cada estudiante
    private static Canal thingSpeak_61343, thingSpeak_66194;
    
    //Vectores que almacenan los minutos correspondientes a cada h�bito a simular (uno por estudiante y h�bito, pues sino 
    //daba problemas de concurrencia de hilos
    private static int[] minutosEstudio1 = new int[]{0,60,120,240}, minutosEstudio2 = new int[]{0,60,120,240},
    		minutosDurmiendo1 = new int[]{0,240,360,480}, minutosDurmiendo2 = new int[]{0,240,360,480},
    		minutosDescanso1 = new int[]{0,20,60,120}, minutosDescanso2 = new int[]{0,20,60,120},
    		minutosTecnologias1 = new int[]{0,120,300}, minutosTecnologias2 = new int[]{0,120,300};
    
    //Variable donde se guardar� el n�mero aleatorio escogido de cada vector
	private static int valorAleatorio;
	
	//Variables usadas para almacenar un valor aleatorio de cada vector
	private static int tiempoEstudiando1, tiempoEstudiando2, tiempoDurmiendo1, tiempoDurmiendo2, 
	tiempoDescansando1, tiempoDescansando2, tiempoUsandoTecnologias1, tiempoUsandoTecnologias2; 
    
    private static int tiempoEstudio1, tiempoEstudio2, tiempoDormir1, tiempoDormir2, tiempoDescansar1, tiempoDescansar2,
    tiempoTecnologias1, tiempoTecnologias2;
    
    private static int estudio = 0; //Campo 1 en ThingSpeak
    private static int dormir = 0; //Campo 2 en ThingSpeak
    private static int descanso_estudio = 0; //Campo 3 en ThingSpeak
    private static int uso_tecnologias = 0; //Campo 4 en ThingSpeak
    
    static Scanner scanner = new Scanner(System.in); //Para recoger texto por consola para hacer el men�
    
    
    public static void main (String [] args){   
    	
    	int opcion = -1;
    	int tiempoEst1, tiempoEst2, tiempoDor1, tiempoDor2, tiempoDesc1, tiempoDesc2, tiempoTecn1, tiempoTecn2;
    	
    	//---------- Estudiante 1 --------------------------------------
    	estudiante1 = new Estudiante("1", "Pablo Ruiz", "UCA");
    	thingSpeak_61343 = new Canal(THINGSPEAK_API_KEY_61343, CHANNEL_ID_61343);
    	
    	//---------- Estudiante 2 --------------------------------------
    	estudiante2 = new Estudiante("2", "Luisa Aguilar", "UPO");
    	thingSpeak_66194 = new Canal(THINGSPEAK_API_KEY_66194, CHANNEL_ID_66194);
    	
    	System.out.println("");
    	System.out.println("");
    	System.out.println("Men� de opciones para simular h�bitos no saludables");
    	System.out.println("-----------------------------------------------------------------");
    	System.out.println("1. Monitorizar informaci�n por un largo tiempo de forma aleatoria");
    	System.out.println("2. Simular tiempo en minutos dedicado al estudio");
    	System.out.println("3. Simular tiempo en minutos dedicado a dormir");
    	System.out.println("4. Simular tiempo en minutos dedicado al descanso del estudio");
    	System.out.println("5. Simular tiempo en minutos dedicado al uso de las tecnolog�as");
    	System.out.println("------------------------------------------------------------------");
    	System.out.print("Seleccione una opci�n del men�: ");
    	
    	opcion = scanner.nextInt();
    	
    	switch (opcion) {
		case 1:
			
			System.out.println("");
			
			//Valores aleatorios de cada vector: Estudiante 1
	    	tiempoEstudiando1 = generarValorAleatorio(minutosEstudio1);
	    	tiempoDurmiendo1 = generarValorAleatorio(minutosDurmiendo1);
	    	tiempoDescansando1 = generarValorAleatorio(minutosDescanso1);
	    	tiempoUsandoTecnologias1 = generarValorAleatorio(minutosTecnologias1);
	    	
	    	//Generaci�n de datos de cada h�bito a simular: Estudiante 1
	    	generarDatosEstudio1(thingSpeak_61343, estudiante1, tiempoEstudiando1);
			generarDatosDormir1(thingSpeak_61343, estudiante1, tiempoDurmiendo1);
			generarDatosDescanso1(thingSpeak_61343, estudiante1, tiempoDescansando1);
			generarDatosTecnologias1(thingSpeak_61343, estudiante1, tiempoUsandoTecnologias1);
			
			//Valores aleatorios de cada vector: Estudiante 2
			tiempoEstudiando2 = generarValorAleatorio(minutosEstudio2);
			tiempoDurmiendo2 = generarValorAleatorio(minutosDurmiendo2);
			tiempoDescansando2 = generarValorAleatorio(minutosDescanso2);
			tiempoUsandoTecnologias2 = generarValorAleatorio(minutosTecnologias2);
			
	    	//Generaci�n de datos de cada h�bito a simular: Estudiante 2
			generarDatosEstudio2(thingSpeak_66194, estudiante2, tiempoEstudiando2);
			generarDatosDormir2(thingSpeak_66194, estudiante2, tiempoDurmiendo2);
			generarDatosDescanso2(thingSpeak_66194, estudiante2, tiempoDescansando2);
			generarDatosTecnologias2(thingSpeak_66194, estudiante2, tiempoUsandoTecnologias2);
			
		break;
		
		case 2:
			System.out.println("");
			System.out.println("---- Estudiante 1 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 1 a estudiar: ");
			tiempoEst1 = scanner.nextInt();
			System.out.println("");
			System.out.println("---- Estudiante 2 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 2 a estudiar: ");
			tiempoEst2 = scanner.nextInt();
			System.out.println("");
			simularEstudio1(thingSpeak_61343, estudiante1, tiempoEst1);
			simularEstudio2(thingSpeak_66194, estudiante2, tiempoEst2);
			
		break;
		
		case 3:
			System.out.println("");
			System.out.println("---- Estudiante 1 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 1 a dormir: ");
			tiempoDor1 = scanner.nextInt();
			System.out.println("");
			System.out.println("---- Estudiante 2 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 2 a dormir: ");
			tiempoDor2 = scanner.nextInt();
			System.out.println("");
			simularDormir1(thingSpeak_61343, estudiante1, tiempoDor1);
			simularDormir2(thingSpeak_66194, estudiante2, tiempoDor2);
			
		break;
		
		case 4:
			System.out.println("");
			System.out.println("---- Estudiante 1 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 1 a descansar del estudio: ");
			tiempoDesc1 = scanner.nextInt();
			System.out.println("");
			System.out.println("---- Estudiante 2 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 2 a descansar del estudio: ");
			tiempoDesc2 = scanner.nextInt();
			System.out.println("");
			simularDescanso1(thingSpeak_61343, estudiante1, tiempoDesc1);
			simularDescanso2(thingSpeak_66194, estudiante2, tiempoDesc2);
			
		break;
		
		case 5:
			System.out.println("");
			System.out.println("---- Estudiante 1 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 1 a usar las tecnolog�as "
					+ "y redes sociales: ");
			tiempoTecn1 = scanner.nextInt();
			System.out.println("");
			System.out.println("---- Estudiante 2 ----");
			System.out.print("Introduzca un n�mero para el tiempo en minutos que dedicar� el Estudiante 2 a usar las tecnolog�as"
					+ "y redes sociales: ");
			tiempoTecn2 = scanner.nextInt();
			System.out.println("");
			simularTecnologias1(thingSpeak_61343, estudiante1, tiempoTecn1);
			simularTecnologias2(thingSpeak_66194, estudiante2, tiempoTecn2);
			
		break;	

		default:
			System.out.println("N�mero no v�lido");
		break;
		
    	}
    		
    }
    

	private static int generarValorAleatorio(int[] minutosHabito) {
    	valorAleatorio =  minutosHabito[(int) Math.floor(Math.random() * minutosHabito.length)];
		return valorAleatorio;
	}
    
	
 private static void generarDatosEstudio1(final Canal thingSpeak11, final Estudiante estudiante11, final int tiempoEstudiando1) {
    	
		switch (tiempoEstudiando1) {
		
		case 0:
			estudio = 0;
			tiempoEstudio1 = 30;
			break;
		
		case 60:
			estudio = 1;
			tiempoEstudio1 = 60;
			break;
			
		case 120:
			estudio = 1;
			tiempoEstudio1 = 120;
			break;
			
		case 240:
			estudio = 1;
			tiempoEstudio1 = 240;
			break;
			
		default:
			estudio = -1;
			tiempoEstudio1 = 0;
			break;
		}
		
		Runnable tareaEstudio1 = new Runnable() {
			
			@Override
			public void run() {
			
				 if(tiempoEstudiando1 == 0){
						estudio = 0;
						System.out.println(estudiante11.get_nombre()+ " cuyo id es " +estudiante11.get_id()+ " no estudiar� durante: " +tiempoEstudio1+ " minutos");
						
					} else{
						System.out.println(estudiante11.get_nombre()+ " cuyo id es " +estudiante11.get_id()+ " estudiar� durante: " +tiempoEstudio1+ " minutos");
					}
					
					System.out.println("Valor campo 1 en ThingSpeak, Estudio : " + estudio);
					thingSpeak11.setDataField(1, estudio);
		        	System.out.println("Simulado y enviado a ThingSpeak");
		        	System.out.println("");
		        	thingSpeak11.sendData();
			}
		};
		
		temporizadorEstudio1.empezarTemporizadorMinutos(tareaEstudio1, tiempoEstudio1, 0);
    
	}
    
 private static void generarDatosEstudio2(final Canal thingSpeak12, final Estudiante estudiante12, final int tiempoEstudiando2) {
    	
		switch (tiempoEstudiando2) {
		
		case 0:
			estudio = 0;
			tiempoEstudio2 = 30;
			break;
		
		case 60:
			estudio = 1;
			tiempoEstudio2 = 60;
			break;
			
		case 120:
			estudio = 1;
			tiempoEstudio2 = 120;
			break;		
		
		case 240:
			estudio = 1;
			tiempoEstudio2 = 240;
			break;	
			
		default:
			estudio = -1;
			tiempoEstudio2 = 0;
			break;
		}
		
		Runnable tareaEstudio2 = new Runnable() {
			
			@Override
			public void run() {
			
				 if(tiempoEstudiando2 == 0){
						estudio = 0;
						System.out.println(estudiante12.get_nombre()+ " cuyo id es " +estudiante12.get_id()+ " no estudiar� durante: " +tiempoEstudio2+ " minutos");
						
					} else{
						System.out.println(estudiante12.get_nombre()+ " cuyo id es " +estudiante12.get_id()+ " estudiar� durante: " +tiempoEstudio2+ " minutos");
					}
					
					System.out.println("Valor campo 1 en ThingSpeak, Estudio : " + estudio);
					thingSpeak12.setDataField(1, estudio);
		        	System.out.println("Simulado y enviado a ThingSpeak");
		        	System.out.println("");
		        	thingSpeak12.sendData();  
			}
		};
		
		temporizadorEstudio2.empezarTemporizadorMinutos(tareaEstudio2, tiempoEstudio2, 0);
    
	}
    
 private static void generarDatosDormir1(final Canal thingSpeak_21, final Estudiante estudiante21, final int tiempoDurmiendo1) {
	 	 
    	switch (tiempoDurmiendo1) {
    	
    	case 0:
    		dormir = 0;
    		tiempoDormir1 = 60;
    		break;
    	
		case 240:
			dormir = 1;
			tiempoDormir1 = 240;
			break;
			
		case 360:
			dormir = 1;
			tiempoDormir1 = 360;
			break;
		
		case 480:
			dormir = 1;
			tiempoDormir1 = 480;
			break;
		
		default:
			dormir = -1;
			tiempoDormir1 = 0;
			break;
		}
		
		Runnable tareaDormir1 = new Runnable() {
			
			@Override
			public void run() {
			
				if(tiempoDurmiendo1 == 0){	
					dormir = 0;
					System.out.println(estudiante21.get_nombre()+ " cuyo id es " +estudiante21.get_id()+ " no dormir� durante: "+tiempoDormir1+ " minutos");

				} else{
					dormir = 1;
					System.out.println(estudiante21.get_nombre()+ " cuyo id es " +estudiante21.get_id()+ " dormir� durante: " +tiempoDormir1+ " minutos");
				}
						
				System.out.println("Valor campo 2 en ThingSpeak, Dormir : " + dormir);
				thingSpeak_21.setDataField(2, dormir);
	        	System.out.println("Simulado y enviado a ThingSpeak");
	        	System.out.println("");
	        	thingSpeak_21.sendData();
	        	
			}
		};
				
		temporizadorDormir1.empezarTemporizadorMinutos(tareaDormir1, tiempoDormir1, 30);
		
	}
	
 private static void generarDatosDormir2(final Canal thingSpeak_22, final Estudiante estudiante22, final int tiempoDurmiendo2) {
	 
	switch (tiempoDurmiendo2) {
	
	case 0:
		dormir = 0;
		tiempoDormir2 = 60;
		break;
	
	case 240:
		dormir = 0;
		tiempoDormir2 = 240;
		break;
		
	case 360:
		dormir = 1;
		tiempoDormir2 = 360;
		break;
	
	case 480:
		dormir = 1;
		tiempoDormir2 = 480;
		break;
	
	default:
		dormir = -1;
		tiempoDormir2 = 0;
		break;
	}
	
	Runnable tareaDormir2 = new Runnable() {
		
		@Override
		public void run() {
		
			if(tiempoDurmiendo2 == 0){	
				dormir = 0;
				System.out.println(estudiante22.get_nombre()+ " cuyo id es " +estudiante22.get_id()+ " no dormir� durante: "+tiempoDormir2+ " minutos");

			} else{
				dormir = 1;
				System.out.println(estudiante22.get_nombre()+ " cuyo id es " +estudiante22.get_id()+ " dormir� durante: " +tiempoDormir2+ " minutos");
			}
					
			System.out.println("Valor campo 2 en ThingSpeak, Dormir : " + dormir);
			thingSpeak_22.setDataField(2, dormir);
        	System.out.println("Simulado y enviado a ThingSpeak");
        	System.out.println("");
        	thingSpeak_22.sendData();	
		}
	};
			
	temporizadorDormir2.empezarTemporizadorMinutos(tareaDormir2, tiempoDormir2, 30);
	
}
 
 private static void generarDatosDescanso1(final Canal thingSpeak_31, final Estudiante estudiante31, final int tiempoDescansando1) {
 	 
 	switch (tiempoDescansando1) {
 	
 	case 0:
 		descanso_estudio = 0;
 		tiempoDescansar1 = 60;
 		break;
 	
	case 20:
		descanso_estudio = 1;
		tiempoDescansar1 = 20;
		break;
			
	case 60:
		descanso_estudio = 1;
		tiempoDescansar1 = 60;
		break;
		
	case 120:
		descanso_estudio = 1;
		tiempoDescansar1 = 120;
		break;	
		
	default:
		descanso_estudio = -1;
		tiempoDescansar1 = 0;
		break;
	}
		
		Runnable tareaDescansar1 = new Runnable() {
			
			@Override
			public void run() {
			
				if(tiempoDescansando1 == 0){	
					descanso_estudio = 0;
					System.out.println(estudiante31.get_nombre()+ " cuyo id es " +estudiante31.get_id()+ " no descansar� del estudio durante: "+tiempoDescansar1+ " minutos");

				} else{
					descanso_estudio = 1;
					System.out.println(estudiante31.get_nombre()+ " cuyo id es " +estudiante31.get_id()+ " descansar� del estudio durante: " +tiempoDescansar1+ " minutos");
				}
						
				System.out.println("Valor campo 3 en ThingSpeak, Descanso_estudio : " + descanso_estudio);
				thingSpeak_31.setDataField(3, descanso_estudio);
	        	System.out.println("Simulado y enviado a ThingSpeak");
	        	System.out.println("");
	        	thingSpeak_31.sendData();
			}
		};
				
		temporizadorDescanso1.empezarTemporizadorMinutos(tareaDescansar1, tiempoDescansar1, 50);
		
	}

 private static void generarDatosDescanso2(final Canal thingSpeak_32, final Estudiante estudiante32, final int tiempoDescansando2) {
 	 
	 	switch (tiempoDescansando2) {
	 	
	 	case 0:
	 		descanso_estudio = 0;
	 		tiempoDescansar2 = 60;
	 		break;
	 	
		case 20:
			descanso_estudio = 1;
			tiempoDescansar2 = 20;
			break;
				
		case 60:
			descanso_estudio = 1;
			tiempoDescansar2 = 60;
			break;
			
		case 1200:
			descanso_estudio = 1;
			tiempoDescansar2 = 120;
			break;	
			
		default:
			descanso_estudio = -1;
			tiempoDescansar2 = 0;
			break;
		}
			
			Runnable tareaDescansar2 = new Runnable() {
				
				@Override
				public void run() {
				
					if(tiempoDescansando2 == 0){	
						descanso_estudio = 0;
						System.out.println(estudiante32.get_nombre()+ " cuyo id es " +estudiante32.get_id()+ " no descansar� del estudio durante: "+tiempoDescansar2+ " minutos");

					} else{
						descanso_estudio = 1;
						System.out.println(estudiante32.get_nombre()+ " cuyo id es " +estudiante32.get_id()+ " descansar� del estudio durante: " +tiempoDescansar2+ " minutos");
					}
							
					System.out.println("Valor campo 3 en ThingSpeak, Descanso_estudio : " + descanso_estudio);
					thingSpeak_32.setDataField(3, descanso_estudio);
		        	System.out.println("Simulado y enviado a ThingSpeak");
		        	System.out.println("");
		        	thingSpeak_32.sendData();
		        	
				}
			};
					
			temporizadorDescanso2.empezarTemporizadorMinutos(tareaDescansar2, tiempoDescansar2, 50);
			
		}

 private static void generarDatosTecnologias1(final Canal thingSpeak_41, final Estudiante estudiante41, final int tiempoUsandoTecnologias1) {
 	 
	 	switch (tiempoUsandoTecnologias1) {
	 	
	 	case 0:
	 		uso_tecnologias = 0;
	 		tiempoTecnologias1 = 60;
	 		break;
	 	
		case 120:
			uso_tecnologias = 1;
			tiempoTecnologias1 = 120;
			break;
				
		case 300:
			uso_tecnologias = 1;
			tiempoTecnologias1 = 300;
			break;
			
		default:
			uso_tecnologias = -1;
			tiempoTecnologias1 = 0;
			break;
		}
			
			Runnable tareaTecnologias1 = new Runnable() {
				
				@Override
				public void run() {
				
					if(tiempoUsandoTecnologias1 == 0){	
						uso_tecnologias = 0;
						System.out.println(estudiante41.get_nombre()+ " cuyo id es " +estudiante41.get_id()+ " no utilizar� las tecnolog�as durante: "+tiempoTecnologias1+ " minutos");

					} else{
						uso_tecnologias = 1;
						System.out.println(estudiante41.get_nombre()+ " cuyo id es " +estudiante41.get_id()+ " utilizar� las tecnolog�as durante: " +tiempoTecnologias1+ " minutos");
					}
							
					System.out.println("Valor campo 4 en ThingSpeak, Uso_tecnologias : " + uso_tecnologias);
					thingSpeak_41.setDataField(4, uso_tecnologias);
		        	System.out.println("Simulado y enviado a ThingSpeak");
		        	System.out.println("");
		        	thingSpeak_41.sendData();
				}
			};
					
			temporizadorTecnologias1.empezarTemporizadorMinutos(tareaTecnologias1, tiempoTecnologias1, 70);
			
		}

 private static void generarDatosTecnologias2(final Canal thingSpeak_42, final Estudiante estudiante42, final int tiempoUsandoTecnologias2) {
 	 
	 	switch (tiempoUsandoTecnologias2) {
	 	
	 	case 0:
	 		uso_tecnologias = 0;
	 		tiempoTecnologias2 = 60;
	 		break;
	 	
		case 120:
			uso_tecnologias = 1;
			tiempoTecnologias2 = 120;
			break;
				
		case 300:
			uso_tecnologias = 1;
			tiempoTecnologias2 = 300;
			break;
			
		default:
			uso_tecnologias = -1;
			tiempoTecnologias2 = 0;
			break;
		}
			
			Runnable tareaTecnologias2 = new Runnable() {
				
				@Override
				public void run() {
				
					if(tiempoUsandoTecnologias2 == 0){	
						uso_tecnologias = 0;
						System.out.println(estudiante42.get_nombre()+ " cuyo id es " +estudiante42.get_id()+ " no utilizar� las tecnolog�as durante: "+tiempoTecnologias2+ " minutos");

					} else{
						uso_tecnologias = 1;
						System.out.println(estudiante42.get_nombre()+ " cuyo id es " +estudiante42.get_id()+ " utilizar� las tecnolog�as durante: " +tiempoTecnologias2+ " minutos");
					}
							
					System.out.println("Valor campo 4 en ThingSpeak, Uso_tecnologias : " + uso_tecnologias);
					thingSpeak_42.setDataField(4, uso_tecnologias);
		        	System.out.println("Simulado y enviado a ThingSpeak");
		        	System.out.println("");
		        	thingSpeak_42.sendData();
		        	System.out.println(" --- Siguiente ejecuci�n ---");
				}
			};
					
			temporizadorTecnologias2.empezarTemporizadorMinutos(tareaTecnologias2, tiempoTecnologias2, 70);
			
		}

 private static void simularEstudio1(final Canal thingSpeakSim, final Estudiante estudianteSim, final int tiempoSim) {
	
	estudio = 1;
	
	Runnable tareaSimularEstudio1 = new Runnable() {
		
		@Override
		public void run() {
			
			System.out.println(estudianteSim.get_nombre()+ " cuyo id es " +estudianteSim.get_id()+ " estudiar� durante: " +tiempoSim+ " minutos");
			System.out.println("Valor campo 1 en ThingSpeak, Estudio : " + estudio);
			thingSpeakSim.setDataField(1, estudio);
	        System.out.println("Simulado y enviado a ThingSpeak");
	        System.out.println("");
	        thingSpeakSim.sendData();
			
		}
	};
	
	temporizadorSimEst.empezarTemporizadorSim(tareaSimularEstudio1, tiempoSim, 0);

}

 private static void simularEstudio2(final Canal thingSpeakSim2, final Estudiante estudianteSim2, final int tiempoSim2) {
	
	estudio = 1;
	
	Runnable tareaSimularEstudio2 = new Runnable() {
		
		@Override
		public void run() {
			
			System.out.println(estudianteSim2.get_nombre()+ " cuyo id es " +estudianteSim2.get_id()+ " estudiar� durante: " +tiempoSim2+ " minutos");
			System.out.println("Valor campo 1 en ThingSpeak, Estudio : " + estudio);
			thingSpeakSim2.setDataField(1, estudio);
	        System.out.println("Simulado y enviado a ThingSpeak");
	        System.out.println("");
	        thingSpeakSim2.sendData();
			
		}
	};
	
	temporizadorSimEst2.empezarTemporizadorSim(tareaSimularEstudio2, tiempoSim2, 0);

}

 private static void simularDormir1(final Canal thingSpeakSimDor, final Estudiante estudianteSimDor, final int tiempoSimDor) {
		
		dormir = 1;
		
		Runnable tareaSimularDormir = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimDor.get_nombre()+ " cuyo id es " +estudianteSimDor.get_id()+ " dormir� durante: " +tiempoSimDor+ " minutos");
				System.out.println("Valor campo 2 en ThingSpeak, Dormir : " + dormir);
				thingSpeakSimDor.setDataField(2, dormir);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimDor.sendData();
				
			}
		};
		
		temporizadorSimDor1.empezarTemporizadorSim(tareaSimularDormir, tiempoSimDor, 0);

	}

 private static void simularDormir2(final Canal thingSpeakSimDor2, final Estudiante estudianteSimDor2, final int tiempoSimDor2) {
		
		dormir = 1;
		
		Runnable tareaSimularDormir2 = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimDor2.get_nombre()+ " cuyo id es " +estudianteSimDor2.get_id()+ " dormir� durante: " +tiempoSimDor2+ " minutos");
				System.out.println("Valor campo 2 en ThingSpeak, Dormir : " + dormir);
				thingSpeakSimDor2.setDataField(2, dormir);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimDor2.sendData();
				
			}
		};
		
		temporizadorSimDor2.empezarTemporizadorSim(tareaSimularDormir2, tiempoSimDor2, 0);

	}

 private static void simularDescanso1(final Canal thingSpeakSimDesc, final Estudiante estudianteSimDesc, final int tiempoSimDesc) {
		
		descanso_estudio = 1;
		
		Runnable tareaSimularDescansoEstudio = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimDesc.get_nombre()+ " cuyo id es " +estudianteSimDesc.get_id()+ " descansar� del estudio durante: " +tiempoSimDesc+ " minutos");
				System.out.println("Valor campo 3 en ThingSpeak, Descanso : " + descanso_estudio);
				thingSpeakSimDesc.setDataField(3, descanso_estudio);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimDesc.sendData();
				
			}
		};
		
		temporizadorSimDesc1.empezarTemporizadorSim(tareaSimularDescansoEstudio, tiempoSimDesc, 0);

	}
 
 private static void simularDescanso2(final Canal thingSpeakSimDesc2, final Estudiante estudianteSimDesc2, final int tiempoSimDesc2) {
		
		descanso_estudio = 1;
		
		Runnable tareaSimularDescansoEstudio2 = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimDesc2.get_nombre()+ " cuyo id es " +estudianteSimDesc2.get_id()+ " descansar� del estudio durante: " +tiempoSimDesc2+ " minutos");
				System.out.println("Valor campo 3 en ThingSpeak, Descanso : " + descanso_estudio);
				thingSpeakSimDesc2.setDataField(3, descanso_estudio);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimDesc2.sendData();
				
			}
		};
		
		temporizadorSimDesc2.empezarTemporizadorSim(tareaSimularDescansoEstudio2, tiempoSimDesc2, 0);

	}

 private static void simularTecnologias1(final Canal thingSpeakSimTecn, final Estudiante estudianteSimTecn, final int tiempoSimTecn) {
		
		uso_tecnologias = 1;
		
		Runnable tareaSimularUsoTecnologias = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimTecn.get_nombre()+ " cuyo id es " +estudianteSimTecn.get_id()+ " utilizar� las tecnolog�as durante: " +tiempoSimTecn+ " minutos");
				System.out.println("Valor campo 4 en ThingSpeak, Uso_tecnologias : " + uso_tecnologias);
				thingSpeakSimTecn.setDataField(4, uso_tecnologias);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimTecn.sendData();
				
			}
		};
		
		temporizadorSimTecn1.empezarTemporizadorSim(tareaSimularUsoTecnologias, tiempoSimTecn, 0);

	}
 
 private static void simularTecnologias2(final Canal thingSpeakSimTecn2, final Estudiante estudianteSimTecn2, final int tiempoSimTecn2) {
		
		uso_tecnologias = 1;
		
		Runnable tareaSimularUsoTecnologias2 = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(estudianteSimTecn2.get_nombre()+ " cuyo id es " +estudianteSimTecn2.get_id()+ " utilizar� las tecnolog�as durante: " +tiempoSimTecn2+ " minutos");
				System.out.println("Valor campo 4 en ThingSpeak, Uso_tecnologias : " + uso_tecnologias);
				thingSpeakSimTecn2.setDataField(4, uso_tecnologias);
		        System.out.println("Simulado y enviado a ThingSpeak");
		        System.out.println("");
		        thingSpeakSimTecn2.sendData();
				
			}
		};
		
		temporizadorSimTecn2.empezarTemporizadorSim(tareaSimularUsoTecnologias2, tiempoSimTecn2, 0);

	}


 
} 

