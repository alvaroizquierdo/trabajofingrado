package clases;

public class Estudiante {
	private String _id;
	private String _nombre;
	private String _universidad;
	
	public Estudiante(String _id, String _nombre, String _universidad) {
		this._id = _id;
		this._nombre = _nombre;
		this._universidad = _universidad;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_nombre() {
		return _nombre;
	}

	public void set_nombre(String _nombre) {
		this._nombre = _nombre;
	}

	public String get_universidad() {
		return _universidad;
	}

	public void set_universidad(String _universidad) {
		this._universidad = _universidad;
	}

	@Override
	public String toString() {
		return "Estudiante [Id = " + _id + ", Nombre = " + _nombre + ", Universidad = " + _universidad + "]";
	}
}