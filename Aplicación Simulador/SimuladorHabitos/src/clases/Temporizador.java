package clases;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Temporizador {
	
	private final ScheduledExecutorService servicio;
	private final long periodo = 60; //periodo de 60 segundos para que se manden los datos cada minuto
	
	public Temporizador(){
		servicio = Executors.newScheduledThreadPool(30);
	}
		
	public void empezarTemporizadorMinutos(Runnable runnable, int tiempo, int retrasoInicial){
				
		ScheduledFuture<?> resultado = servicio.scheduleWithFixedDelay(runnable, retrasoInicial, periodo, TimeUnit.SECONDS); //ejecuta el runnable cada 60 segundos con un retraso inicial de 0 (para que lo haga directamente)
		
		Runnable cancelar = new Runnable() {
			
			@Override
			public void run() {
				
				resultado.cancel(true); //Para que cuando finalice el tiempo, no se siga ejecutando la tarea
				
				if(servicio != null){
					servicio.shutdown(); //Para que no se quede en espera una vez finalizada la tarea
				}
				
			}
		};
		servicio.schedule(cancelar, tiempo, TimeUnit.MINUTES); //Se ejecuta la tarea por el tiempo correspondiente en minutos 
	}
	
	public void empezarTemporizadorSim(Runnable runnable, int tiempo, int retrasoInicial){
		
		ScheduledFuture<?> resultado = servicio.scheduleAtFixedRate(runnable, retrasoInicial, periodo, TimeUnit.SECONDS); //ejecuta el runnable cada 60 segundos con un retraso inicial de 0 (para que lo haga directamente)
		
		Runnable cancelar = new Runnable() {
			
			@Override
			public void run() {
				
				resultado.cancel(true); //Para que cuando finalice el tiempo, no se siga ejecutando la tarea
				
				if(servicio != null){
					servicio.shutdown(); //Para que no se quede en espera una vez finalizada la tarea
				}
				
			}
		};
		servicio.schedule(cancelar, tiempo, TimeUnit.MINUTES); //Se ejecuta la tarea por el tiempo correspondiente en minutos 
	}

	

}
