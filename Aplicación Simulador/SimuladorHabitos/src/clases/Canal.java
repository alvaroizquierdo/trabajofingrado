package clases;

import com.angryelectron.thingspeak.Channel;
import com.angryelectron.thingspeak.Entry;
import com.angryelectron.thingspeak.ThingSpeakException;
import com.mashape.unirest.http.exceptions.UnirestException;


/*
 * Clase que sirve para establecer los campos de ThingSpeak y de enviarlos con sus valores correspondientes
 * Utiliza la librer�a thingspeak-1.1.1.jar
 */

public class Canal 
{
	private Channel channel;
	private Entry entry;
		
	public Canal(String apiKey, int channelId)
	{
		channel = new Channel(channelId, apiKey);
		entry = new Entry();
	}
	
	public String getChannelName()
	{
		try {
			return channel.getChannelFeed().getChannelName();
		} catch (UnirestException | ThingSpeakException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public void setDataField(int field, int value)
	{
		entry.setField(field, String.valueOf(value));
	}
	
	public String getStatus()
	{
		return entry.getStatus();
	}
	
	
	public void sendData()
	{
		try {
			
			channel.update(entry);

			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			
		} catch (UnirestException e) {
			e.printStackTrace();
		} catch (ThingSpeakException e) {
			e.printStackTrace();
		}
	}
}
