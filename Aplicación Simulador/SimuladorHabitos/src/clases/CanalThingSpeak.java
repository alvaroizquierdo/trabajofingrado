package clases;

import com.angryelectron.thingspeak.Channel;
import com.angryelectron.thingspeak.Entry;
import com.angryelectron.thingspeak.ThingSpeakException;
import com.mashape.unirest.http.exceptions.UnirestException;

public class CanalThingSpeak {
	
	private Channel channel;
	private Entry entry;
		
	public CanalThingSpeak(int idCanal, String claveEscritura){
		channel = new Channel(idCanal, claveEscritura);
		entry = new Entry();
	}
	
	public void establecerValorCampo(int campo, int valor){
		entry.setField(campo, Integer.toString(valor));
	}
	
	public void enviarDatos(){
		
			try {
				channel.update(entry);
			} catch (UnirestException e) {
				e.printStackTrace();
			} catch (ThingSpeakException e) {
				e.printStackTrace();
			}
			
	}

}
