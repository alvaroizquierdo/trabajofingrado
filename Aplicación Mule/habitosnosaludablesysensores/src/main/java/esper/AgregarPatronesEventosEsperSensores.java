package esper;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.lifecycle.Callable;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.StatementAwareUpdateListener;


public class AgregarPatronesEventosEsperSensores implements Callable { 
	
	public Object onCall(final MuleEventContext eventContext) throws Exception {
		
		final Object payload = eventContext.getMessage().getPayload();
	    final EPStatement pattern = EsperUtils.agregarPatronEvento(payload.toString());
		System.out.println("\n--- Patron de evento agregado al motor Esper desde el fichero:\n" + payload.toString());
		
		StatementAwareUpdateListener genericListener = new StatementAwareUpdateListener() {
			
			@SuppressWarnings("deprecation")
			public void update(EventBean[] newComplexEvents, EventBean[] oldComplexEvents, 
					EPStatement detectedEventPattern, EPServiceProvider serviceProvider) {
				
				if (newComplexEvents != null) {
					
					try {
					
						Map<String, Object> messageProperties = new HashMap<String, Object>();
						
						messageProperties.put("eventPatternName", detectedEventPattern.getName());												
						messageProperties.put("timestamp", new Date((long) detectedEventPattern.getTimeLastStateChange()));
						messageProperties.put("eventFlowPatternName", detectedEventPattern.getEventType().getName());
						messageProperties.put("text", detectedEventPattern.getText());
						
						System.out.println("  -> Agregando propiedades para el email de la alerta de los sensores");						
						
						eventContext.getMuleContext().getClient().dispatch("ConsumidorEventosComplejosSensoresVM", newComplexEvents[0].getUnderlying(), messageProperties);
						
						String info = "\n\n***********************************************************************************"
								+ "\n Nombre del patron del evento detectado: " + detectedEventPattern.getName()
								+ "\n Texto del patron del evento detectado: " + detectedEventPattern.getText()
								+ "\n\n\n"
								+ "\n Tipo del patron del evento detectado: " + detectedEventPattern.getEventType()
								+ "\n Identificador del tipo del patron del evento detectado: " +detectedEventPattern.getEventType().getEventTypeId()
								+ "\n***********************************************************************************\n\n\n";
						
						System.out.println(" -> Evento detectado de los datos de los sensores: \n " + info);
						System.out.println(info);
						
						eventContext.getMuleContext().getClient().dispatch("ConsumidorEventosComplejosSensoresVM", newComplexEvents[0].getUnderlying(), messageProperties);

																			
					} catch (MuleException e) {
						e.printStackTrace();
					}	
				}
				
		     }
		};
		
		EsperUtils.agregarListener(genericListener, pattern);	
				
		return payload.toString();
	}	

}


