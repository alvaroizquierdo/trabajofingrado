package esper;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.mule.api.annotations.param.Payload;

public class EnviarEventoEsper {

	
	@SuppressWarnings("unchecked")
	public synchronized void enviarEventoEsper(@Payload Map<String, Object> eventMap) { 
		
		String eventTypeName = (String) eventMap.keySet().toArray()[0];
		System.out.println("--- Nombre del tipo de evento: " + eventTypeName);
		
		Map<String, Object> eventPayload = new HashMap<String, Object>();
		eventPayload = (Map<String, Object>) eventMap.get(eventTypeName);
		
		Map<String, Object> eventPayloadTypeMap = new HashMap<String, Object>();
		eventPayloadTypeMap = conseguirTipoEvento(eventPayload);
		System.out.println("--- eventPayloadTypeMap: " + eventPayloadTypeMap);

		if (!EsperUtils.comprobarTipoEvento(eventTypeName)) {
			EsperUtils.agregarTipoEvento(eventTypeName, eventPayloadTypeMap);
			System.out.println("***" + eventTypeName + " tipo de evento a�adido al motor Esper");
		}
								
		EsperUtils.enviarEvento(eventPayload, eventTypeName);
		System.out.println("\nEnviando evento '" + eventTypeName + "' a Esper");
	}

	
	private Map<String, Object> conseguirTipoEvento(Map<String, Object> eventMap) {
		   
		Map<String, Object> eventTypeMap = new LinkedHashMap<String, Object>();

		for (String key : eventMap.keySet()) {
			
			Object value = eventMap.get(key);
               
			if (value instanceof Map) {               
				@SuppressWarnings("unchecked")
				Map<String, Object> nestedEventProperty = conseguirTipoEvento((Map<String, Object>) value);
				eventTypeMap.put(key, nestedEventProperty);
				
			} else {
				eventTypeMap.put(key, value.getClass());
			}
		}

		return eventTypeMap;
	}
}