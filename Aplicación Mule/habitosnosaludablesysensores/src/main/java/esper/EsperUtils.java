package esper;

import java.util.Map;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.StatementAwareUpdateListener;


public class EsperUtils {
	
	private static EPServiceProvider epService;
			
	public static EPServiceProvider getService() {
		
		synchronized(EsperUtils.class) {
			
			if (epService == null) {
				
				Configuration config = new Configuration();										
				epService = EPServiceProviderManager.getDefaultProvider(config);
				System.out.println("Arrancando motor Esper");
			
			}
		}
		
		return epService;
	}

	public static boolean comprobarTipoEvento(String eventTypeName) {
		return getService().getEPAdministrator().getConfiguration().isEventTypeExists(eventTypeName);
	}
	
	public static void agregarTipoEvento(String eventTypeName, Map<String, Object> eventTypeMap) {
		getService().getEPAdministrator().getConfiguration().addEventType(eventTypeName, eventTypeMap);
	}

	public static EPStatement agregarPatronEvento(String eventPattern) throws Exception {
		return getService().getEPAdministrator().createEPL(eventPattern);
	}
	
	public static void agregarListener(StatementAwareUpdateListener listener, EPStatement eventPattern) throws Exception {
		eventPattern.addListener(listener);
	}
	
	public static void enviarEvento(Map<String,Object> eventMap, String eventTypeName) {
		getService().getEPRuntime().sendEvent(eventMap, eventTypeName);
	}

}
