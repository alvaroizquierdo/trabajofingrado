package esper;

import java.io.File;
import java.util.Comparator;


public class ComparadorNombres implements Comparator<Object>{

	@Override
	public int compare(Object o1, Object o2) {
		
		if (o1 instanceof File && o2 instanceof File){
			
			File f = (File) o1;
			File f1 = (File) o2;
			
			return f.getName().compareTo(f1.getName());
		}
		return 0;
	}
}
