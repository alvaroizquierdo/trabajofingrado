package transformer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


@ContainsTransformerMethods
public class TransformadorJSONSimulador extends AbstractMessageTransformer {
		
	@Override
	public synchronized Map<String, Object> transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		Map<String, Object> eventMap = new LinkedHashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
				
		try {
		
			jsonNode = mapper.readTree(message.getPayloadAsString());
			Map<String, Object> eventPayload = new LinkedHashMap<String, Object>();

			//Atributos comunes de todos los canales		
			eventPayload.put("idHabito", jsonNode.get("channel").get("id").asInt());
			eventPayload.put("nombreHabito", jsonNode.get("channel").get("name").getTextValue());
			eventPayload.put("tiempoActualizadoHabito", jsonNode.get("feeds").path(0).get("created_at").getTextValue());
									
			eventPayload.put("estudio", jsonNode.get("feeds").path(0).get("field1").asInt());
			eventPayload.put("dormir", jsonNode.get("feeds").path(0).get("field2").asInt());
			eventPayload.put("descanso_estudio", jsonNode.get("feeds").path(0).get("field3").asInt());
			eventPayload.put("uso_tecnologias", jsonNode.get("feeds").path(0).get("field4").asInt());
			
			eventMap.put("EventoHabito", eventPayload);
			
			System.out.println("  -> Evento 'EventoHabito' generado desde el simulador " + eventMap);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
			
		} catch (JsonParseException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
						
		return eventMap;
	}
	
}