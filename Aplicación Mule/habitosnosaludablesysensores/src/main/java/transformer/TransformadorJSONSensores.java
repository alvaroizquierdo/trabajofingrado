package transformer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


@ContainsTransformerMethods
public class TransformadorJSONSensores extends AbstractMessageTransformer {
		
	@Override
	public synchronized Map<String, Object> transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {

		Map<String, Object> eventMap = new LinkedHashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
				
		try {
			jsonNode = mapper.readTree(message.getPayloadAsString());
			Map<String, Object> eventPayload = new LinkedHashMap<String, Object>();

			//Atributos comunes de todos los canales		
			eventPayload.put("idSensor", jsonNode.get("channel").get("id").asInt());
			eventPayload.put("nombreSensor", jsonNode.get("channel").get("name").getTextValue());
			eventPayload.put("tiempoActualizadoSensor", jsonNode.get("feeds").path(0).get("created_at").getTextValue());
			
			eventPayload.put("iluminancia", jsonNode.get("feeds").path(0).get("field1").asDouble());
			eventPayload.put("exactitud_sensor", jsonNode.get("feeds").path(0).get("field2").asInt());
			eventPayload.put("nivel_bateria", jsonNode.get("feeds").path(0).get("field3").asInt());
			eventPayload.put("voltaje", jsonNode.get("feeds").path(0).get("field4").asInt());
			eventPayload.put("temperatura_bateria", jsonNode.get("feeds").path(0).get("field5").asInt());
			eventPayload.put("porcentaje_cpu", jsonNode.get("feeds").path(0).get("field6").asDouble());
			eventPayload.put("actividad_realizada", jsonNode.get("feeds").path(0).get("field7").asInt());
			eventPayload.put("porcentaje_actividad", jsonNode.get("feeds").path(0).get("field8").asInt());

			eventMap.put("EventoSensor", eventPayload);
			
			System.out.println("  -> Evento 'EventoSensor' generado desde los sensores del movil " + eventMap);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
			
		} catch (JsonParseException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		    									
		return eventMap;
	}
	
}